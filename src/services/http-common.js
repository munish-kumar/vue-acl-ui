/**
 * http-common carries the base instance that allows you to
 * share a common base URL and configuration across all
 * calls to the instance
 */

import axios from 'axios'
import Auth from './auth'
import { constants } from '@/constant'

export const HTTP = axios.create({
  baseURL: constants.baseURL,
  headers: {
    Accept: 'application/json'
  }
})

/**
 * Register an axios request interceptor to attach authorization token
 * with each request from client
 */
HTTP.interceptors.request.use(config => {
  config.headers.authorization = `Bearer ${Auth.getToken()}`
  return config
},
error => Promise.reject(error))
