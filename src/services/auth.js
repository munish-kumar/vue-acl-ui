import { constants } from '@/constant'

export default {

  storeToken (token) {
    window.localStorage.setItem(constants.API_TOKEN, token)
  },

  getToken () {
    return window.localStorage.getItem(constants.API_TOKEN)
  },

  isLoggedIn () {
    return this.getToken()
  },

  logout () {
    window.localStorage.removeItem(constants.API_TOKEN)
  }

}
