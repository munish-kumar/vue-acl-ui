import { constants } from '@/constant'

export default {
  /**
   * stringify user permissions and store them in local storage
   * stored data structure: { 'USER_EMAIL': [PERMISSIONS] }
   * e.g : { 'abc@gmail.com': [INBOX, COMPOSE, SENT_MAILS] }
   * @param {*} user
   * @param {*} permissions
   */
  storeUserPermissions (user, permissions) {
    let userPermissions = window.localStorage.getItem(constants.USER_PERMISSIONS)
    // convert stringify object to JS object
    userPermissions = userPermissions ? JSON.parse(userPermissions) : {}
    userPermissions[user] = permissions
    // store in local storage
    window.localStorage.setItem(constants.USER_PERMISSIONS, JSON.stringify(userPermissions))
  },

  /**
   * Return user permissions
   * @param {*} email
   */
  getUserPermissions (email) {
    // return all system permissions if user is admin
    if (email === constants.adminEmail) {
      return this.getAllPermissions()
    }
    // get stored permissions from local storage
    let userPermissions = window.localStorage.getItem(constants.USER_PERMISSIONS)
    // make sure data fetched from local storage is a JS object
    userPermissions = userPermissions ? JSON.parse(userPermissions) : {}
    // return default permissions if user's permissions are not found in local storage
    return userPermissions[email] ? userPermissions[email] : this.getDefaultPermissions()
  },

  // return permission that are default and assigned to each user
  getDefaultPermissions () {
    return [constants.PERMISSIONS.public, constants.PERMISSIONS.inbox]
  },

  // returns all system permissions
  getAllPermissions () {
    return Object.keys(constants.PERMISSIONS).map((k) => constants.PERMISSIONS[k])
  }
}
