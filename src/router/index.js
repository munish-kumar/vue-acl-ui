import VueRouter from 'vue-router'

import Login from '@/components/Login.vue'
import Signup from '@/components/Signup.vue'

import Dashboard from '@/components/Dashboard.vue'
import Settings from '@/components/Settings.vue'
import Inbox from '@/components/Inbox.vue'
import SentMails from '@/components/SentMails.vue'
import Trash from '@/components/Trash.vue'
import PermissionDenied from '@/components/PermissionDenied'

import Auth from '@/services/auth'
import { constants } from '@/constant'

const routes = [
  {
    path: '/',
    redirect: '/login',
    meta: {
      permission: constants.PERMISSIONS.public
    }
  },
  {
    path: '/login',
    component: Login,
    meta: {
      permission: constants.PERMISSIONS.public
    }
  },
  {
    path: '/signup',
    component: Signup,
    meta: {
      permission: constants.PERMISSIONS.public
    }
  },
  {
    path: '/dashboard',
    component: Dashboard,
    children: [
      {
        path: '',
        component: Inbox,
        meta: {
          permission: constants.PERMISSIONS.inbox
        }
      },
      {
        path: 'settings',
        component: Settings,
        meta: {
          permission: constants.PERMISSIONS.acl
        }
      },
      {
        path: 'sent-mails',
        component: SentMails,
        meta: {
          permission: constants.PERMISSIONS.sentMails
        }
      },
      {
        path: 'trash',
        component: Trash,
        meta: {
          permission: constants.PERMISSIONS.trash
        }
      },
      {
        path: 'error',
        component: PermissionDenied,
        meta: {
          permission: constants.PERMISSIONS.public
        }
      }
    ],
    meta: {
      requiresAuth: true,
      permission: [constants.PERMISSIONS.inbox]
    }
  }
]

// create a router instance
export const router = new VueRouter({
  routes
})

/**
 * Check whether user is authenticated to access routes that have
 * requiresAuth meta field on them
 */
router.beforeEach((to, from, next) => {
  next()
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!Auth.isLoggedIn()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})
