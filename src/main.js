// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import { router } from './router'
import VueRouter from 'vue-router'
/**
 * Importing VueMaterial whole package is not recommneded, It will
 * add the unnecessary code in the final bundled package.
 * Keeping POC and time frame in mind this is imported to save time
 */
import VueMaterial from 'vue-material'
import Acl from 'vue-acl'
import Toaster from 'v-toaster'

import store from './store'
import { constants } from './constant'
import UserPermissions from '@/services/user-permissions'

import Vuetable from 'vuetable-2/src/components/Vuetable'
import VuetablePagination from 'vuetable-2/src/components/VuetablePagination'

// application components

import Login from '@/components/Login.vue'
import Signup from '@/components/Signup.vue'

import Dashboard from '@/components/Dashboard.vue'
import Settings from '@/components/Settings.vue'
import Inbox from '@/components/Inbox.vue'
import SentMails from '@/components/SentMails.vue'
import Trash from '@/components/Trash.vue'
import PermissionDenied from '@/components/PermissionDenied'
import Spinner from '@/components/Spinner'
import DataTable from '@/components/data-table/Table'

import './style.scss'

Vue.config.productionTip = false

window.Vue = Vue

// installs
Vue.use(VueRouter)
Vue.use(VueMaterial)
Vue.use(Toaster, {timeout: 3000})

// Visit https://github.com/leonardovilarinho/vue-acl for documentation
Vue.use(Acl, {
  router: router,
  init: UserPermissions.getDefaultPermissions(),
  save: true,
  fail: '/dashboard/error'
})

/**
 * constants might be used in every component of the application
 * so its better to add them in vue's prototype chain rather to
 * import them in each file
 */
Vue.prototype.$constants = constants
// Register components

Vue.component('Vuetable', Vuetable)
Vue.component('VuetablePagination', VuetablePagination)

// public components
Vue.component('app-login', Login)
Vue.component('app-signup', Signup)

// components that can be accessed by authenticated users
Vue.component('app-dashboard', Dashboard)
Vue.component('app-settings', Settings)
Vue.component('app-inbox', Inbox)
Vue.component('app-sent-mails', SentMails)
Vue.component('app-trash-mails', Trash)
Vue.component('app-permission-denied', PermissionDenied)
Vue.component('app-spinner', Spinner)
Vue.component('app-datatable', DataTable)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
