import Vue from 'vue'
import Vuex from 'vuex'
import * as types from './mutation-types'
import { constants } from '../constant'

Vue.use(Vuex)

// root state object.
// each Vuex instance is just a single state tree.
const state = {
  selectedPage: constants.PAGE.inbox,
  userList: [],
  loggedInUser: {}
}

// mutations are operations that actually mutates the state.
// each mutation handler gets the entire state tree as the
// first argument, followed by additional payload arguments.
// mutations must be synchronous and can be recorded by plugins
// for debugging purposes.
const mutations = {

  [types.SET_PAGE] (state, page) {
    state.selectedPage = page
  },

  [types.SET_USER_LIST] (state, userList) {
    state.userList = userList
  },

  [types.SET_LOGGED_IN_USER] (state, user) {
    state.loggedInUser = user
  }
}

// actions are functions that cause side effects and can involve
// asynchronous operations.
const actions = {

  setPage ({ commit }, page) {
    commit(types.SET_PAGE, page)
  },

  setUserList ({ commit }, users) {
    commit(types.SET_USER_LIST, users)
  },

  setLoggedInUser ({ commit }, user) {
    commit(types.SET_LOGGED_IN_USER, user)
  }
}

// getters are functions
const getters = {
  selectedPage: state => state.selectedPage,
  loggedInUser: state => state.loggedInUser,
  userList: state => state.userList
}

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
