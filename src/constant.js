export const constants = {

  baseURL: '',

  PERMISSIONS: {
    sentMails: 'SENT_MAILS',
    inbox: 'INBOX',
    compose: 'COMPOSE',
    acl: 'ACL',
    trash: 'TRASH',
    public: 'PUBLIC'
  },

  USER_PERMISSIONS: 'user_permissions',

  API_TOKEN: 'access_token',

  adminEmail: 'admin@gmail.com',

  PAGE: {
    inbox: 'Inbox',
    sent_mails: 'Sent Mails',
    trash: 'Trash',
    acl: 'ACL',
    permission_denied: 'Permission Denied!'
  }
}
